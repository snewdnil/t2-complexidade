// Henrique Teruo Eihara
// RA: 490016
// Trabalho 2 - Complexidade - 31/05/2014

#include <stdio.h>
#include <stdlib.h>

int main(){
  int N;
  int i,i2,i3=1;
  int * matriz = NULL;
  int entrada;
  int flag = 0;

  scanf("%d",&N);

  while(N){
    matriz = malloc(sizeof(int*)*(N*N));

    for(i = 0;i< N;i++){
      for(i2 = 0;i2 < i3;i2++){
        scanf("%d", &entrada);
        matriz[(i*N)+i2] = entrada;
      }
      i3++;
    }

    for(i = N-1;i > 0;i--){
      for(i2 = i3-1;i2 > 1;i2--){
        if(matriz[(i*N)+i2-1] > matriz[(i*N)+i2-2]){
          matriz[((i-1)*N)+i2-2] += matriz[(i*N)+i2-1];
        }else{
          matriz[((i-1)*N)+i2-2] += matriz[((i)*N)+i2-2];
        }
      }
    }

    printf("%d\n",matriz[0]);
    i3 = 1;
    free(matriz);
    scanf("%d",&N);
  }
}
